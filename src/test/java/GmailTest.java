import driver.DriverManager;
import model.Message;
import model.Users;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import po.AlertPO;
import po.GmailHomePagePO;
import po.GmailLoginPO;
import util.JAXBParser;
import util.PropertiesReader;

public class GmailTest {
    Message message = JAXBParser.getMessageModelFromXML(PropertiesReader.getInstance().getMessageXMLPath());
    Users users = JAXBParser.getUsersFromXML(PropertiesReader.getInstance().getUsersXMLRath());

    @BeforeMethod
    public void setUp() {
        DriverManager.getDriver().get(PropertiesReader.getInstance().getGmailURL());
    }


    @DataProvider(parallel = true)
    public Object[][] users() {
        Object[][] objects = new Object[users.getUsersList().size()][2];
        for (int i = 0; i < users.getUsersList().size(); i++) {
            objects[i][0] = users.getUsersList().get(i).getEmail();
            objects[i][1] = users.getUsersList().get(i).getPassword();
        }
        return objects;
    }

    @Test(dataProvider = "users")
    public void testGmail(String userEmail, String password) {
        GmailLoginPO gmailLoginPO = new GmailLoginPO(DriverManager.getDriver());
        GmailHomePagePO gmailHomePagePO = new GmailHomePagePO(DriverManager.getDriver());
        AlertPO alertPO = new AlertPO(DriverManager.getDriver());
        gmailLoginPO.inputLoginAndClick(userEmail);
        gmailLoginPO.inputPasswordAndClick(password);
        gmailHomePagePO.clickOnWriteAMessageButton();
        gmailHomePagePO.writeAMessageTo(message.getInvalidEmail());
        gmailHomePagePO.writeASubjectOfMessage(message.getSubject());
        gmailHomePagePO.writeAMessage(message.getMessage());
        gmailHomePagePO.sendAMessage();
        Assert.assertTrue(alertPO.getAlertMessage().contains(message.getInvalidEmail()));
        alertPO.clickOkOnAlertPopup();
        alertPO.clickCloseOnAlertPopup();
        gmailHomePagePO.clickOnWriteAMessageButton();
        gmailHomePagePO.writeAMessageTo(message.getReceiver());
        gmailHomePagePO.writeASubjectOfMessage(message.getSubject());
        gmailHomePagePO.writeAMessage(message.getMessage());
        gmailHomePagePO.sendAMessage();
        gmailHomePagePO.openSentMessage();
        Assert.assertTrue(gmailHomePagePO.getMessageDetails().contains(message.getMessage()));
        Assert.assertTrue(gmailHomePagePO.getMessageDetails().contains("voronovskatest"));
    }

    @AfterMethod
    public void tearDown() {
        DriverManager.quit();
    }
}

