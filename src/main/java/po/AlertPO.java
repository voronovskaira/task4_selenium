package po;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class AlertPO extends BasePage {

    public AlertPO(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@role='alertdialog']/div[2]")
    private WebElement alertMessageLine;

    @FindBy(xpath = "//button[@name='ok']")
    private WebElement okButtonOnAlertPopup;

    @FindBy(xpath = "//img[@alt='Закрити']")
    private WebElement closeButtonOnAlertPopup;

    public String getAlertMessage() {
        waitElementToBeClickable(alertMessageLine);
        return alertMessageLine.getText();
    }

    public void clickOkOnAlertPopup() {
        waitElementToBeClickable(okButtonOnAlertPopup);
        okButtonOnAlertPopup.click();
    }

    public void clickCloseOnAlertPopup() {
        waitElementToBeClickable(closeButtonOnAlertPopup);
        new Actions(driver).clickAndHold(closeButtonOnAlertPopup).release().perform();
    }
}
